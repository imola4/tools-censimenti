use std::ffi::CString;
use std::os::raw::c_char;

#[no_mangle]
pub extern "C" fn censimenti_parser_parse(file: *const u8, len: usize) -> *mut c_char {
    let file = unsafe { std::slice::from_raw_parts(file, len) };

    match censimenti_parser::Parser::new(file).and_then(|x| x.parse_json()) {
        Ok(r) => CString::new(r).unwrap().into_raw(),
        Err(e) => CString::new(format!("{{\"error\": \"{}\"}}", e.replace('"', "\\\"")))
            .unwrap()
            .into_raw(),
    }
}

#[no_mangle]
pub extern "C" fn censimenti_parser_parse_free(s: *mut c_char) {
    unsafe {
        if s.is_null() {
            return;
        }
        drop(CString::from_raw(s))
    };
}
