use std::fmt::Display;

use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub enum Gender {
    Male,
    Female,
}

impl Display for Gender {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Gender::Male => f.write_str("M"),
            Gender::Female => f.write_str("F"),
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Person {
    pub name: String,
    pub surname: String,
    pub date_of_birth: Option<(u16, u8, u8)>,
    pub place_of_birth: Option<String>,
    pub fiscal_code: Option<String>,
    pub sex: Option<Gender>,
    pub contacts: Option<Contacts>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Enrollment {
    pub _id: String,
    pub id: Option<u32>,
    pub details: Person,
    pub address: Option<PostalAddress>,
    pub parents: [Option<Person>; 2],
    pub privacy: (bool, bool, bool),
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PostalAddress {
    pub street: String,
    pub town: String,
}

impl PostalAddress {
    pub fn get_street_parts(&self) -> Option<(&str, &str, &str)> {
        let street_type = 0..self.street.find(' ')?;
        let mut street_name = 0..0;
        street_name.start = street_type.end + 1;
        street_name.end = street_name.start + self.street[street_name.start..].find(' ')?;
        let spaces_to_add = self.street.split_whitespace().count() - 3; // One whitespace at the beginning, the one we just found and one at the end
        for _ in 0..spaces_to_add {
            street_name.end += 1;
            street_name.end =
                street_name.end + self.street[street_name.end..].find(' ').unwrap_or_default();
        }
        let house_number = street_name.end + 1..;
        while !&self.street[street_name.end - 1..street_name.end]
            .chars()
            .next()?
            .is_alphabetic()
        {
            street_name.end -= 1;
        }
        Some((
            &self.street[street_type],
            &self.street[street_name],
            &self.street[house_number],
        ))
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Contacts {
    pub phone: Option<String>,
    pub email: Option<String>,
}

impl Contacts {
    pub fn flatten(self) -> Option<Self> {
        if self.email.is_none() && self.phone.is_none() {
            None
        } else {
            Some(self)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn split_address_full() {
        let address = PostalAddress {
            street: "Via Mario Rossi, 3".to_string(),
            town: "".to_string(),
        };
        let result = address.get_street_parts();
        assert_eq!(result, Some(("Via", "Mario Rossi", "3")));
    }
    #[test]
    fn split_address_full_no_comma() {
        let address = PostalAddress {
            street: "Via Mario Rossi 3".to_string(),
            town: "".to_string(),
        };
        let result = address.get_street_parts();
        assert_eq!(result, Some(("Via", "Mario Rossi", "3")));
    }
    #[test]
    fn split_address() {
        let address = PostalAddress {
            street: "Via Rossi 3".to_string(),
            town: "".to_string(),
        };
        let result = address.get_street_parts();
        assert_eq!(result, Some(("Via", "Rossi", "3")));
    }
    #[test]
    fn split_address_empty() {
        let address = PostalAddress {
            street: "".to_string(),
            town: "".to_string(),
        };
        let result = address.get_street_parts();
        assert_eq!(result, None);
    }
    #[test]
    fn split_address_invalid() {
        let address = PostalAddress {
            street: "Pizza 123".to_string(),
            town: "".to_string(),
        };
        let result = address.get_street_parts();
        assert_eq!(result, None);
    }
}
