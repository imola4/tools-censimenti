use calamine::Reader;

use censimenti_types::*;

#[derive(Default, Debug)]
struct Mapping {
    name: Option<usize>,
    surname: Option<usize>,
    date_of_birth: Option<usize>,
    place_of_birth: Option<usize>,
    fiscal_code: Option<usize>,
    sex: Option<usize>,
    agesci_id: Option<usize>,
    street: std::ops::Range<usize>,
    town: std::ops::Range<usize>,
    privacy_a: Option<usize>,
    privacy_b: Option<usize>,
    privacy_c: Option<usize>,
    parent_a: std::ops::Range<usize>,
    parent_b: std::ops::Range<usize>,
    email: Option<usize>,
    phone: Option<usize>,
}

impl From<&[calamine::DataType]> for Mapping {
    fn from(value: &[calamine::DataType]) -> Self {
        fn range_extend<T>(range: &mut std::ops::Range<T>, value: T)
        where
            T: PartialOrd + PartialEq + Default + Copy,
        {
            if range.start == range.end && range.start == Default::default() {
                range.start = value;
                range.end = value;
            } else if value < range.start {
                range.start = value;
            } else if value > range.end {
                range.end = value;
            }
        }
        let mut mapping = Self::default();
        for (i, cell) in value.iter().enumerate().take_while(|(_, x)| !x.is_empty()) {
            if let Some(heading) = cell.get_string() {
                match heading {
                    "Codice Socio" => mapping.agesci_id = Some(i),
                    "Cognome" => mapping.surname = Some(i),
                    "Nome" => mapping.name = Some(i),
                    "Sesso" => mapping.sex = Some(i),
                    "Data Nascita" => mapping.date_of_birth = Some(i),
                    "Luogo nascita" => mapping.place_of_birth = Some(i),
                    "Indirizzo" => range_extend(&mut mapping.street, i),
                    "Città res" => range_extend(&mut mapping.town, i),
                    "CF" => mapping.fiscal_code = Some(i),
                    "Cognome G1" => range_extend(&mut mapping.parent_a, i),
                    "Nome G1" => range_extend(&mut mapping.parent_a, i),
                    "Data nascita G1" => range_extend(&mut mapping.parent_a, i),
                    "Luogo nascita G1" => range_extend(&mut mapping.parent_a, i),
                    "Sesso G1" => range_extend(&mut mapping.parent_a, i),
                    "CF G1" => range_extend(&mut mapping.parent_a, i),
                    "Mail G1" => range_extend(&mut mapping.parent_a, i),
                    "Cell G1" => range_extend(&mut mapping.parent_a, i),
                    "Cognome G2" => range_extend(&mut mapping.parent_b, i),
                    "Nome G2" => range_extend(&mut mapping.parent_b, i),
                    "Data nascita G2" => range_extend(&mut mapping.parent_b, i),
                    "Luogo nascita G2" => range_extend(&mut mapping.parent_b, i),
                    "Sesso G2" => range_extend(&mut mapping.parent_b, i),
                    "CF G2" => range_extend(&mut mapping.parent_b, i),
                    "Mail G2" => range_extend(&mut mapping.parent_b, i),
                    "Cell G2" => range_extend(&mut mapping.parent_b, i),
                    "2.a" => mapping.privacy_a = Some(i),
                    "2.b" => mapping.privacy_b = Some(i),
                    "2.c" => mapping.privacy_c = Some(i),
                    "CodiceSocio" => mapping.agesci_id = Some(i),
                    "DataNascita" => mapping.date_of_birth = Some(i),
                    "ComuneNascita" => mapping.place_of_birth = Some(i),
                    "CodiceFiscale" => mapping.fiscal_code = Some(i),
                    "Civico" => range_extend(&mut mapping.street, i),
                    "ComuneResidenza" => range_extend(&mut mapping.town, i),
                    "ProvinciaResidenza" => range_extend(&mut mapping.town, i),
                    "Cap" => range_extend(&mut mapping.town, i),
                    "Informativa2a" => mapping.privacy_a = Some(i),
                    "Informativa2b" => mapping.privacy_b = Some(i),
                    "Informativa 2c" => mapping.privacy_c = Some(i),
                    "Cellulare" => mapping.phone = Some(i),
                    "Email" => mapping.email = Some(i),
                    _ => {}
                }
            }
        }
        mapping.street.end += 1;
        mapping.town.end += 1;
        mapping.parent_a.end += 1;
        mapping.parent_b.end += 1;
        mapping
    }
}

pub struct Parser {
    workbook: calamine::Range<calamine::DataType>,
    mapping: Mapping,
}
impl Parser {
    fn parse_date(&self, date: &calamine::DataType) -> Option<(u16, u8, u8)> {
        match date {
            calamine::DataType::DateTimeIso(date) => {
                let mut date = date.split('-');
                Some((
                    date.next()?.parse().ok()?,
                    date.next()?.parse().ok()?,
                    date.next()?.parse().ok()?,
                ))
            }
            calamine::DataType::String(date) => {
                let date_part = date.split(' ').next()?;
                let mut date_components = date_part.split('/');
                let d = date_components.next()?.parse().ok()?;
                let m = date_components.next()?.parse().ok()?;
                let y = date_components.next()?.parse().ok()?;
                Some((y, m, d))
            }
            _ => None,
        }
    }

    fn parse_string(&self, value: &calamine::DataType) -> Option<String> {
        Some(value.get_string()?.trim().to_owned())
    }

    fn parse_string_concat(&self, value: &[calamine::DataType]) -> Option<String> {
        Some(
            value
                .iter()
                .flat_map(|x| x.as_string())
                .map(|x| x.trim().to_owned())
                .collect::<Vec<_>>()
                .join(" "),
        )
    }

    fn parse_gender(&self, value: &calamine::DataType) -> Option<Gender> {
        Some(match self.parse_string(value)?.as_str() {
            "M" => Gender::Male,
            "F" => Gender::Female,
            _ => None?,
        })
    }

    fn parse_id(&self, value: &calamine::DataType) -> Option<u32> {
        Some(match value {
            calamine::DataType::Int(x) => *x as u32,
            calamine::DataType::Float(x) => *x as u32,
            calamine::DataType::String(x) => x.parse().ok()?,
            _ => None?,
        })
    }

    fn parse_contacts(
        &self,
        email: Option<&calamine::DataType>,
        phone: Option<&calamine::DataType>,
    ) -> Option<Contacts> {
        Contacts {
            phone: phone.and_then(|phone| match phone {
                calamine::DataType::Int(x) => Some(format!("{x}")),
                calamine::DataType::Float(x) => Some(format!("{}", *x as u64)),
                calamine::DataType::String(x) => Some(x.to_owned()),
                _ => None,
            }),
            email: email.and_then(|email| self.parse_string(email)),
        }
        .flatten()
    }
    fn parse_parent(&self, cells: &[calamine::DataType]) -> Option<Person> {
        if cells.len() < 7 {
            None
        } else {
            Some(Person {
                name: self.parse_string(&cells[1])?,
                surname: self.parse_string(&cells[0])?,
                date_of_birth: self.parse_date(&cells[2]),
                place_of_birth: self.parse_string(&cells[3]),
                fiscal_code: self.parse_string(&cells[5]),
                sex: self.parse_gender(&cells[4]),
                contacts: self.parse_contacts(Some(&cells[6]), Some(&cells[7])),
            })
        }
    }

    fn parse_row(&self, row: &[calamine::DataType]) -> Option<Enrollment> {
        let member = Person {
            name: self.parse_string(&row[self.mapping.name?])?,
            surname: self.parse_string(&row[self.mapping.surname?])?,
            date_of_birth: self
                .mapping
                .date_of_birth
                .and_then(|i| self.parse_date(&row[i])),
            place_of_birth: self
                .mapping
                .place_of_birth
                .and_then(|i| self.parse_string(&row[i])),
            fiscal_code: self
                .mapping
                .fiscal_code
                .and_then(|i| self.parse_string(&row[i])),
            sex: self.mapping.sex.and_then(|i| self.parse_gender(&row[i])),
            contacts: self.parse_contacts(
                self.mapping.email.map(|i| &row[i]),
                self.mapping.phone.map(|i| &row[i]),
            ),
        };
        Some(Enrollment {
            _id: {
                let data: String = member
                    .name
                    .chars()
                    .chain(member.surname.chars())
                    .filter(|x| x.is_alphabetic())
                    .map(|x| x.to_lowercase().collect::<String>())
                    .collect();
                let data = data + ".censimenti_parser.eutampieri.eu";
                uuid::Uuid::new_v5(&uuid::Uuid::NAMESPACE_DNS, data.as_bytes()).to_string()
            },
            id: self.parse_id(&row[self.mapping.agesci_id?]),
            details: member,
            address: (|| {
                Some(PostalAddress {
                    street: self.parse_string_concat(&row[self.mapping.street.clone()])?,
                    town: self.parse_string_concat(&row[self.mapping.town.clone()])?,
                })
            })(),
            parents: [
                self.parse_parent(&row[self.mapping.parent_a.clone()]),
                self.parse_parent(&row[self.mapping.parent_b.clone()]),
            ],
            privacy: (
                self.parse_bool(&row[self.mapping.privacy_a?])?,
                self.parse_bool(&row[self.mapping.privacy_b?])?,
                self.parse_bool(&row[self.mapping.privacy_c?])?,
            ),
        })
    }

    fn parse_bool(&self, cell: &calamine::DataType) -> Option<bool> {
        match cell {
            calamine::DataType::Int(x) => Some(*x == 1),
            calamine::DataType::Float(x) => Some(*x == 1.0),
            calamine::DataType::String(x) => Some(x == "TRUE" || x == "Si"),
            calamine::DataType::Bool(x) => Some(*x),
            calamine::DataType::Empty => Some(false),
            _ => None,
        }
    }

    pub fn parse_json(&self) -> Result<String, String> {
        serde_json::to_string(&self.parse()).map_err(|e| format!("{:?}", e))
    }
    pub fn parse(&self) -> Vec<Enrollment> {
        self.workbook
            .rows()
            .skip(1)
            .flat_map(|x| self.parse_row(x))
            .collect::<Vec<_>>()
    }
    pub fn new(workbook: &[u8]) -> Result<Self, String> {
        let mut workbook = calamine::open_workbook_auto_from_rs(std::io::Cursor::new(workbook))
            .map_err(|e| format!("{:?}", e))?;
        Ok(Self {
            workbook: workbook.worksheets()[0].1.clone(),
            mapping: Mapping::from(&workbook.worksheets()[0].1[0]),
        })
    }
}
