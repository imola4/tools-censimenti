use std::io::Write;

fn main() {
    let file_name = std::env::args().nth(1).expect("Provide a filename");
    let workbook = std::fs::read(file_name).expect("File not found");
    let parser = censimenti_parser::Parser::new(&workbook).unwrap();
    std::io::stdout()
        .write_all(
            parser
                .parse_json()
                .expect("Serialization failed")
                .as_bytes(),
        )
        .expect("Could not write JSON to stdout");
}
