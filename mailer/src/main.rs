use std::str::FromStr;

use clap::Parser;
use lettre::Transport;

const NUMBER_OF_DOWNLOADS: u8 = 2;
const LINK_DURATION: usize = 24 * 3600;

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Signature
    #[arg(long)]
    signature: String,

    /// Filename to open
    #[arg(short, long)]
    filename: String,

    /// Sender address
    #[arg(long)]
    from: String,

    /// CC address
    #[arg(long)]
    cc: Option<String>,
    /// CC address
    #[arg(long)]
    smtp_server: String,
    /// CC address
    #[arg(long)]
    smtp_username: String,
    /// CC address
    #[arg(long)]
    smtp_password: String,
}

fn bool_to_str(b: bool) -> &'static str {
    if b {
        "sì"
    } else {
        "no"
    }
}

fn main() {
    let args = Args::parse();

    let bytes = std::fs::read(&args.filename).expect("Could not open file.");
    let parser = censimenti_parser::Parser::new(&bytes).expect("Could not read file.");
    let enrollments = parser.parse();
    let checker = censimenti_checker::CheckingSession::default();
    let ffsend_client = ffsend_api::client::ClientConfigBuilder::default()
        .timeout(Some(std::time::Duration::from_secs(2)))
        .transfer_timeout(Some(std::time::Duration::from_secs(10)))
        .basic_auth(None)
        .build()
        .expect("failed to create network client configuration");
    let client = ffsend_client.client(true);
    for enrollment in enrollments {
        let mut message = format!("Cari genitori di {0} {1},\nvi inviamo i dati di {0} in nostro possesso, chiedendovi di verificarli, confermare i consensi rilasciati in precedenza e riconsegnarci il modulo.\n", enrollment.details.name, enrollment.details.surname);
        let errors = checker.check_enrollment(&enrollment);
        let path = std::path::PathBuf::from_str("form.pdf").unwrap();
        std::fs::write(
            &path,
            censimenti_form_filler::fill_form(&enrollment, "https://pdf-filler.cptech.ovh/fill")
                .unwrap(),
        )
        .unwrap();
        let file = ffsend_api::action::upload::Upload::new(
            ffsend_api::api::Version::V3,
            ffsend_api::reqwest::Url::parse("https://send.vis.ee/").unwrap(),
            path.clone(),
            Some(format!(
                "privacy_{}{}.pdf",
                enrollment.details.name, enrollment.details.surname
            )),
            None,
            Some(ffsend_api::action::params::ParamsData {
                download_limit: Some(NUMBER_OF_DOWNLOADS),
                expiry_time: Some(LINK_DURATION),
            }),
        )
        .invoke(&client, None)
        .unwrap();
        std::fs::remove_file(path).unwrap();
        message += &format!(
            "Il modulo è scaricabile al seguente link: {}\nPer garantire la vostra privacy, il link è valido per {} download e scade il {}",
            file.download_url(true),
            NUMBER_OF_DOWNLOADS, file.expire_at().format("%d/%m/%Y alle %H:%M")
        );
        message += &format!("\nCi risultano rilasciati i seguenti consensi:\n- 2.a (obbligatorio): {}\n- 2.b (facoltativo): {}\n- 2.c (facoltativo): {}", bool_to_str(enrollment.privacy.0), bool_to_str(enrollment.privacy.1), bool_to_str(enrollment.privacy.2));
        if !errors.is_empty() {
            message += "\nVi chiediamo anche di controllare le seguenti informazioni:";
            for (level, e) in errors {
                match level {
                    censimenti_checker::LogLevel::Error | censimenti_checker::LogLevel::Warning => {
                        message += &format!("\n- {e}");
                    }
                    _ => {}
                }
            }
        }
        message += "\n\nCordiali saluti, \n";
        message += &args.signature;
        let email = {
            let mut message_builder =
                lettre::Message::builder().from((&args.from).parse().unwrap());
            if let Some(p) = &enrollment.parents[0] {
                if let Some(c) = &p.contacts {
                    if let Some(e) = &c.email {
                        message_builder = message_builder.to(e.parse().unwrap());
                    }
                }
            }
            if let Some(p) = &enrollment.parents[1] {
                if let Some(c) = &p.contacts {
                    if let Some(e) = &c.email {
                        message_builder = message_builder.to(e.parse().unwrap());
                    }
                }
            }
            if let Some(cc) = &args.cc {
                message_builder.cc(cc.parse().unwrap())
            } else {
                message_builder
            }
        }
        .subject(&format!("Moduli per privacy ({})", enrollment.details.name))
        .header(lettre::message::header::ContentType::TEXT_PLAIN)
        .body(message)
        .unwrap();

        let creds = lettre::transport::smtp::authentication::Credentials::new(
            args.smtp_username.clone(),
            args.smtp_password.clone(),
        );

        // Open a remote connection to gmail
        let mailer = lettre::SmtpTransport::relay(&args.smtp_server)
            .unwrap()
            .credentials(creds)
            .build();

        // Send the email
        match mailer.send(&email) {
            Ok(_) => println!("Email sent successfully!"),
            Err(e) => panic!("Could not send email: {e:?}"),
        }
    }
}
