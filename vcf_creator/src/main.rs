use std::io::Write;

fn create_vcf(element: censimenti_types::Enrollment) -> String {
    use std::fmt::Write;
    let mut result = format!(
        "BEGIN:VCARD\r\nVERSION:3.0\r\nN:{};{};;;\r\nFN:{1} {0}",
        element.details.surname, element.details.name
    );
    if let Some((y, m, d)) = element.details.date_of_birth {
        result
            .write_fmt(format_args!("\r\nBDAY:{y}-{m:02}-{d:02}"))
            .unwrap();
    }
    /*if let Some(sex) = element.details.sex {
        result.write_fmt(format_args!("\r\nGENDER:{sex}")).unwrap();
    }*/
    for (i, parent) in element.parents.into_iter().flatten().enumerate() {
        if let Some(contacts) = parent.contacts {
            if let Some(phone) = &contacts.phone {
                result
                    .write_fmt(format_args!(
                        "\r\np{i}phone.TEL:{}\r\np{i}phone.X-ABLabel:{} {}",
                        phone, parent.name, parent.surname
                    ))
                    .unwrap();
            }
            if let Some(email) = &contacts.email {
                result
                    .write_fmt(format_args!(
                        "\r\np{i}email.EMAIL;TYPE=internet:{}\r\np{i}email.X-ABLabel:{} {}",
                        email, parent.name, parent.surname
                    ))
                    .unwrap();
            }
        }
    }
    result + "\r\nEND:VCARD"
}

fn main() {
    let censimenti: Vec<censimenti_types::Enrollment> =
        serde_json::from_reader(std::io::stdin()).expect("Cannot parse from stdin");
    std::io::stdout()
        .write_all(
            &censimenti
                .into_iter()
                .map(create_vcf)
                .collect::<Vec<_>>()
                .join("\r\n")
                .into_bytes(),
        )
        .expect("Could not write contacts to stdout");
    std::io::stdout().write_all(b"\r\n").unwrap();
}
