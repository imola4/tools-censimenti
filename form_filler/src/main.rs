use censimenti_form_filler::fill_form;

fn main() {
    let censimenti: Vec<censimenti_types::Enrollment> =
        serde_json::from_reader(std::io::stdin()).expect("Cannot parse from stdin");
    let pdf_filler_url = std::env::var("PDF_FILLER_URL")
        .unwrap_or_else(|_| "https://pdf-filler.cptech.ovh/fill".to_string());
    for c in censimenti.into_iter() {
        let pdf = fill_form(&c, &pdf_filler_url).unwrap();
        std::fs::write(&format!("{}{}.pdf", c.details.surname, c.details.name), pdf);
        //println!("{}", form.)
    }
}
