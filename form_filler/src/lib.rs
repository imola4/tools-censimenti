const PRIVACY_PDF: &[u8] = include_bytes!("../privacy_minori.pdf");

fn generate_node(name: &str, bytes: Vec<u8>) -> mime_multipart::Node {
    mime_multipart::Node::Part(mime_multipart::Part {
        headers: {
            let mut headers = mime_multipart::Headers::new();
            headers.set(mime_multipart::ContentDisposition {
                disposition: mime_multipart::DispositionType::Ext("form-data".to_string()),
                parameters: vec![
                    mime_multipart::DispositionParam::Ext("name".to_string(), name.to_string()),
                    mime_multipart::DispositionParam::Ext("filename".to_string(), name.to_string()),
                ],
            });
            headers.set_raw("Content-Type", vec![b"application/octect-stream".to_vec()]);
            headers
        },
        body: bytes,
    })
}

fn field<S: AsRef<str>>(name: &str, value: S) -> String {
    let value = value.as_ref();
    format!("<field name=\"{name}\"><value>{value}</value></field>\n")
}

pub fn fill_form(
    c: &censimenti_types::Enrollment,
    pdf_filler_url: &str,
) -> Result<Vec<u8>, String> {
    let xfdf = {
        let mut xfdf = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><xfdf xmlns=\"http://ns.adobe.com/xfdf/\" xml:space=\"preserve\"><fields>".to_string();
        if let Some(id) = c.id {
            for (i, digit) in format!("{id:07}").chars().enumerate() {
                xfdf += &field(&format!("CodSocio{}", i + 1), digit.to_string());
            }
        }
        xfdf += &field(
            "NomeAssociato",
            format!("{} {}", c.details.name, c.details.surname),
        );
        if let Some((y, m, d)) = c.details.date_of_birth {
            xfdf += &field("DataNascitaAssociato", format!("{d:02}/{m:02}/{y}"));
        }
        if let Some(pob) = &c.details.place_of_birth {
            xfdf += &field("LuogoNascitaAssociato", pob);
        }
        if let Some(cf) = &c.details.fiscal_code {
            xfdf += &field("CFAssociato", cf);
        }
        if let Some(address) = &c.address {
            if let Some((_, a_name, a_number)) = address.get_street_parts() {
                xfdf += &field("NCAssociato", a_number);
                xfdf += &field("IndirizzoAssociato", a_name);
            } else {
                xfdf += &field("IndirizzoAssociato", &address.street);
            }
            xfdf += &field("CittaAssociato", &address.town);
        }

        for (i, g) in c
            .parents
            .iter()
            .enumerate()
            .flat_map(|(i, x)| x.as_ref().map(|x| (i, x)))
        {
            let c = &g.contacts;
            xfdf += &field(
                &format!("NomeG{}", i + 1),
                format!("{} {}", g.name, g.surname),
            );
            if let Some((y, m, d)) = g.date_of_birth {
                xfdf += &field(
                    &format!("DataNascitaG{}", i + 1),
                    format!("{d:02}/{m:02}/{y}"),
                );
            }
            if let Some(pob) = &g.place_of_birth {
                xfdf += &field(&format!("LuogoNascitaG{}", i + 1), pob);
            }
            if let Some(cf) = &g.fiscal_code {
                xfdf += &field(&format!("CFG{}", i + 1), cf);
            }
            if let Some(phone) = c.as_ref().and_then(|x| x.phone.clone()) {
                xfdf += &field(&format!("CellG{}", i + 1), phone)
            }
            if let Some(email) = c.as_ref().and_then(|x| x.email.clone()) {
                xfdf += &field(&format!("EmailG{}", i + 1), email)
            }
        }
        xfdf += "</fields></xfdf>";
        xfdf
    };
    let boundary = mime_multipart::generate_boundary();
    let (form, form_length) = {
        let mut form = Vec::<u8>::new();
        let bytes = mime_multipart::write_multipart(
            &mut form,
            &boundary,
            &vec![
                generate_node("pdf", PRIVACY_PDF.to_vec()),
                generate_node("form", xfdf.into_bytes()),
            ],
        )
        .unwrap();
        (form, bytes)
    };
    {
        let mut pdf = Vec::<u8>::new();
        ureq::post(pdf_filler_url)
            .set(
                "Content-Type",
                &format!("multipart/form-data; boundary={}", &boundary.escape_ascii()),
            )
            .send_bytes(&form[0..form_length])
            .map_err(|e| {
                e.into_response()
                    .map(|x| x.into_string().unwrap_or_default())
                    .unwrap_or_default()
            })?
            .into_reader()
            .read_to_end(&mut pdf)
            .unwrap();
        Ok(pdf)
    }
}
