fn main() {
    let censimenti: Vec<censimenti_types::Enrollment> =
        serde_json::from_reader(std::io::stdin()).expect("Cannot parse from stdin");
    simple_logger::SimpleLogger::new().env().init().unwrap();
    let checker = censimenti_checker::CheckingSession::default();

    log::info!("Trovati {} elementi.", censimenti.len());
    censimenti
        .iter()
        .map(|x| checker.check_enrollment(x))
        .flatten()
        .for_each(|x| match x.0 {
            censimenti_checker::LogLevel::Error => log::error!("{}", x.1),
            censimenti_checker::LogLevel::Warning => log::warn!("{}", x.1),
            censimenti_checker::LogLevel::Info => log::info!("{}", x.1),
            censimenti_checker::LogLevel::Debug => log::debug!("{}", x.1),
            censimenti_checker::LogLevel::Trace => log::trace!("{}", x.1),
        });
}
