use codice_fiscale::CodiceFiscale;

#[derive(Default)]
pub struct CheckingSession {
    logs: std::cell::RefCell<Vec<(LogLevel, String)>>,
}

#[derive(Clone)]
pub enum LogLevel {
    Error,
    Warning,
    Info,
    Debug,
    Trace,
}

impl CheckingSession {
    fn log_level(&self, level: LogLevel, message: String) {
        self.logs.borrow_mut().push((level, message));
    }
    fn log_error(&self, message: String) {
        self.log_level(LogLevel::Error, message)
    }
    fn log_warn(&self, message: String) {
        self.log_level(LogLevel::Warning, message)
    }
    fn log_info(&self, message: String) {
        self.log_level(LogLevel::Info, message)
    }
    fn check_cf(&self, cf: &str, dob: (u16, u8, u8), gender: &censimenti_types::Gender) -> bool {
        fn fix_date(date: &str) -> (u16, u8, u8) {
            let mut date = date.split('-');
            (
                date.next().unwrap().parse().unwrap(),
                date.next().unwrap().parse().unwrap(),
                date.next().unwrap().parse().unwrap(),
            )
        }
        fn check_gender(a: codice_fiscale::Gender, b: &censimenti_types::Gender) -> bool {
            matches!(
                (a, b),
                (codice_fiscale::Gender::M, censimenti_types::Gender::Male)
                    | (codice_fiscale::Gender::F, censimenti_types::Gender::Female)
            )
        }
        CodiceFiscale::parse(cf)
            .map(|x| x.get_person_data().clone())
            .map(|x| fix_date(&x.birthdate) == dob && check_gender(x.gender, gender))
            .unwrap_or_default()
    }

    fn check_person(&self, prefix: &str, p: &censimenti_types::Person) {
        if p.fiscal_code.is_none() {
            self.log_warn(format!("[{}] nessun codice fiscale", prefix));
        }
        if let Some(dob) = p.date_of_birth {
            if let Some(cf) = &p.fiscal_code {
                if let Some(gender) = &p.sex {
                    if !self.check_cf(cf, dob, gender) {
                        self.log_error(format!("[{}] codice fiscale errato", prefix));
                    }
                } else {
                    self.log_warn(format!(
                        "[{}] Impossibile controllare il codice fiscale: sesso mancante!",
                        prefix
                    ))
                }
            }
        } else {
            self.log_error(format!("[{}] nessuna data di nascita", prefix));
        }
        if p.place_of_birth.is_none() {
            self.log_error(format!("[{}] nessun luogo di nascita", prefix));
        }
    }

    pub fn check_enrollment(
        &self,
        enrollment: &censimenti_types::Enrollment,
    ) -> Vec<(LogLevel, String)> {
        self.logs.borrow_mut().clear();
        if enrollment.id.is_none() {
            self.log_warn(format!(
                "[{} {}] nessun codice censimento",
                enrollment.details.name, enrollment.details.surname
            ));
        }
        self.check_person(
            &format!("{} {}", enrollment.details.name, enrollment.details.surname),
            &enrollment.details,
        );
        if enrollment.address.is_none() {
            self.log_warn(format!(
                "[{} {}] nessun indirizzo",
                enrollment.details.name, enrollment.details.surname
            ));
        }

        if let Some(p) = &enrollment.parents[0] {
            self.check_person(
                &format!(
                    "{} {} -> {} {} (G1)",
                    enrollment.details.name, enrollment.details.surname, p.name, p.surname
                ),
                p,
            );
            if p.contacts
                .as_ref()
                .map(|x| x.phone.is_none())
                .unwrap_or_default()
            {
                self.log_warn(format!(
                    "[{} {} -> {} {} (G1)] Nessun numero di telefono",
                    enrollment.details.name, enrollment.details.surname, p.name, p.surname
                ));
            }
            if p.contacts
                .as_ref()
                .map(|x| x.email.is_none())
                .unwrap_or_default()
            {
                self.log_error(format!(
                    "[{} {} -> {} {} (G1)] Nessun indirizzo di posta elettronica",
                    enrollment.details.name, enrollment.details.surname, p.name, p.surname
                ));
            }
        } else {
            self.log_error(format!(
                "[{} {}] Dati genitore 1 mancanti",
                enrollment.details.name, enrollment.details.surname
            ));
        }

        if let Some(p) = &enrollment.parents[1] {
            self.check_person(
                &format!(
                    "{} {} -> {} {} (G2)",
                    enrollment.details.name, enrollment.details.surname, p.name, p.surname
                ),
                p,
            );
            if p.contacts
                .as_ref()
                .map(|x| x.phone.is_none())
                .unwrap_or_default()
            {
                self.log_warn(format!(
                    "[{} {} -> {} {} (G2)] Nessun numero di telefono",
                    enrollment.details.name, enrollment.details.surname, p.name, p.surname
                ));
            }
            if p.contacts
                .as_ref()
                .map(|x| x.email.is_none())
                .unwrap_or_default()
            {
                self.log_error(format!(
                    "[{} {} -> {} {} (G2)] Nessun indirizzo di posta elettronica",
                    enrollment.details.name, enrollment.details.surname, p.name, p.surname
                ));
            }
        } else {
            self.log_error(format!(
                "[{} {}] Dati genitore 2 mancanti",
                enrollment.details.name, enrollment.details.surname
            ));
        }

        // Privacy checks
        if !enrollment.privacy.0 {
            self.log_error(format!(
            "[{} {}] Manca il consenso al punto 2.a dell'informativa sulla privacy (OBBLIGATORIO)",
            enrollment.details.name,
            enrollment.details.surname
        ));
        }
        if !enrollment.privacy.1 {
            self.log_info(format!(
                "[{} {}] Non è stato fornito il consenso facoltativo all'utilizzo delle immagini",
                enrollment.details.name, enrollment.details.surname
            ));
        }
        if !enrollment.privacy.2 {
            self.log_info(format!(
                "[{} {}] Non è stato fornito il consenso facoltativo all'utilizzo delle immagini da parte di terzi. (punto 2.c)",
                enrollment.details.name,
                enrollment.details.surname
            ));
        }
        self.logs.borrow().clone()
    }
}
